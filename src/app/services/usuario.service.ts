import { ThisReceiver } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsuarioDataI, UsuarioI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


//LOGIN 
loading:boolean = false;
nombre_usuario!: string;
user!:UsuarioI


//REGISTRO 
RegisterList!:UsuarioDataI[];


  constructor(private _snackbar :MatSnackBar) { }


  snackbar(number: number):void {
    if(number === 1){
      
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
    } else if (number === 2){
      
    this._snackbar.open('Bienvenido!', this.nombre_usuario,{
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
    }
  }
//LOGIN DATOS
logearDatos(user:UsuarioI){
  console.log(user);

  this.user = user;
  console.log(this.user);

  sessionStorage.setItem('Usuario_nombre', this.user.usuario);
  console.log('aqui ya se subio al session estorage');

  let nombre =sessionStorage.getItem('Usuario_nombre')
  console.log(nombre, 'get');
  
  return nombre;

}


mostrarUser(){
  let nombre = sessionStorage.getItem('Usuario_nombre')
  console.log(nombre);
  return nombre
 
}



cerrarSesion(){
  sessionStorage.removeItem('Usuario_nombre');
}


ObtenerLS(){
  let listaderegistros = JSON.parse(localStorage.getItem("Registros") || '');

  
  this.RegisterList  = listaderegistros
  return listaderegistros
}

//REGISTRO

  agregarUsuario(usuario: UsuarioDataI): void {
    console.log(usuario);
    if(localStorage.getItem('Registros')=== null){
      this.RegisterList = [];
      this.RegisterList.push(usuario);
      localStorage.setItem('Registros', JSON.stringify(this.RegisterList))
    }else {
      this.RegisterList=JSON.parse(localStorage.getItem('Registros')|| '');
      this.RegisterList.unshift(usuario);
      localStorage.setItem('Registros',JSON.stringify(this.RegisterList))
    }
    
    
  }

  
}
