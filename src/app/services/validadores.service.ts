import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {
  registros!: RegistroI

  constructor() { }

  




  passwordsIguales(pass1Name: string, pass2Name: string): ValidationErrors | null {
    return (controls: AbstractControl) => {
      const pass1Control = controls.get(pass1Name)?.value;
      const pass2Control = controls.get(pass2Name)?.value;

      if(pass1Control === pass2Control){
        return controls.get(pass2Name)?.setErrors(null);
      } else {
        return controls.get(pass2Name)?.setErrors({ noEsIgual: true});
      }
    };
  }


  usuarioExistente(user: string){

    const lista: UsuarioDataI[] = JSON.parse(localStorage.getItem("Registros") || '');
    console.log(lista);

    const Usuario = { usuario: 'jperez', password: '123'
  }

  }



  existeUsuario(control: FormControl): Promise<any> | Observable<any>{

    const lista: UsuarioDataI[] = JSON.parse(localStorage.getItem("Registros") || '');
    console.log(lista);


    return new Promise((resolve, reject) => {
      console.log('ingreso ExisteUsuario');
      
      setTimeout(() => {
        if (lista.find(element => element.usuario === control.value)){
          console.log('nombre repetido');
          resolve({ existe: true});
          
        } else {
          console.log('nombre nuevo');
          resolve(null);
        }
      }, 0);
    });
  }


  existeEmail(control: FormControl): Promise<any> | Observable<any> {

    const lista: UsuarioDataI[] = JSON.parse(localStorage.getItem("Citas") || '');
    // console.log(lista);
  
    return new Promise((resolve, reject) =>{
      console.log('hola'); //se imprime para ver si esta entrando
      setTimeout(() => {
        // console.log(this.listaRegistro, 'alba');
        
        if (lista.find(element => element.correo=== control.value)){
          console.log('email repetido');
          resolve({ existe: true});
          
        } else {
          console.log('email nuevo');
          resolve(null);
        }
      }, 3500)
      
    })
  }



}

interface RegistroI{
  nombre: string,
  apellido: string,
  correo: string,
  usuario:string,
  pass1: string,
  pass2: string
}
