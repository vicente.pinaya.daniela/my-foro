import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarIzqComponent } from './sidebar-izq.component';

describe('SidebarIzqComponent', () => {
  let component: SidebarIzqComponent;
  let fixture: ComponentFixture<SidebarIzqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarIzqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidebarIzqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
