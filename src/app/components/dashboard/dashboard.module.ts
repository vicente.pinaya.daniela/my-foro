import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavbarComponent } from './navbar/navbar.component';

//ANGULAR MATERIAL 
import { SharedModule } from '../shared/shared.module';
import { UsuariosComponent } from './usuarios/usuarios.component';


//EDITOR DE TEXTO 
import { NgxEditorModule } from 'ngx-editor';
import { FormsModule } from '@angular/forms';

import { SidebarDerComponent } from './sidebar-der/sidebar-der.component';
import { SidebarIzqComponent } from './sidebar-izq/sidebar-izq.component';
import { TemasComponent } from './temas/temas.component';
import { CrearForoComponent } from './temas/crear-foro/crear-foro.component';
import { VerForoComponent } from './temas/ver-foro/ver-foro.component';



@NgModule({
  declarations: [
    DashboardComponent,
    InicioComponent,
    NavbarComponent,
    UsuariosComponent,
    SidebarDerComponent,
    SidebarIzqComponent,
    TemasComponent,
    CrearForoComponent,
    VerForoComponent
   
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    FormsModule,
     //editor
     NgxEditorModule.forRoot({
      locals: {
        //menu
        bold: 'Bold',
        italic: 'Italic',
        code: 'Code', 
        blockquote: 'Blockquote',
        underline: 'Underline',
        strike: 'Strike',
        bullet_list: 'Bullet List',
        ordered_list: 'Ordered List',
        heading: 'Heading',
        h1: 'Header 1',
        h2: 'Header 2',
        h3: 'Header 3',
        h4: 'Header 4',
        h5: 'Header 5',
        h6: 'Header 6',
        align_left: 'Left Align',
        align_center: 'Center Align',
        align_right: 'Right Align',
        align_justify: 'Justify',
        text_color: 'Text Color',
        background_color: 'Background Color',
        // popups, forms, others...
        url: 'URL',
        text: 'Text',
        penInNewTab: 'Open in new tab',
        insert: 'Insert',
        altText: 'Alt Text',
        title: 'Title',
        remove: 'Remove',        
      },

    })

  ]
})
export class DashboardModule { }
