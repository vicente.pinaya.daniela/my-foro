import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from 'src/app/interfaces/menu';
import { MenuService } from 'src/app/services/menu.service';
import { UsuarioService } from 'src/app/services/usuario.service';
//Menu

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userName!:string;
  mostrar = false 

  menu:Menu[]=[];

  constructor(private _menuService:MenuService,
    private _usuariosSvc : UsuarioService,
    private router : Router) {

      let nombre = this._usuariosSvc.mostrarUser()
      if (nombre === null){
        this.userName = ''
      } else {
        console.log("mostrooo");
        
        this.userName = nombre
        this.mostrar = true
      }
          
          console.log(nombre);

     }


  ngOnInit(): void {
    this.cargarMenu();
  }

  cargarMenu():void{
    this._menuService.getMenu().subscribe(data =>{
      console.log(data);
      this.menu=data;
      
    });
  }


  /* ////////user */
  goToInicioSession(){
    this.router.navigate(['/login'])
  }

  cerrarSesion(){
    this._usuariosSvc.cerrarSesion();
    this.router.navigate(['/dashboard'])
  }

}


// <div class="navbar">
//   <!-- <span class="logo">My APP</span> -->
 
//   <div class="button">

//     <button mat-button *ngFor="let item of menu" [routerLink]="item.redirect">{{ item.nombre }}</button>

//     <button class="btn" mat-raised-button routerLink ="/registro">Registrarse</button>
//     <button class="btn" mat-raised-button routerLink ="/login">Iniciar Sesion <mat-icon>logout</mat-icon></button>
//     <button class="btn" mat-raised-button (click)="cerrarSesion()">Cerrar Sesion<mat-icon>logout</mat-icon></button>

//   </div>

  
// </div>