import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { CrearForoComponent } from './temas/crear-foro/crear-foro.component';
import { TemasComponent } from './temas/temas.component';
import { VerForoComponent } from './temas/ver-foro/ver-foro.component';


import { UsuariosComponent } from './usuarios/usuarios.component';


const routes: Routes = [
  {path: '', component: DashboardComponent,
    children:
        [
          {path: '', component:InicioComponent},
          {path:'usuarios', component: UsuariosComponent},
          {path:'temas', component:TemasComponent},
          {path:'crear_foro/:id', component: CrearForoComponent},
          {path:'ver_foro/:id', component: VerForoComponent}
        ] }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
