import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarDerComponent } from './sidebar-der.component';

describe('SidebarDerComponent', () => {
  let component: SidebarDerComponent;
  let fixture: ComponentFixture<SidebarDerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarDerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidebarDerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
