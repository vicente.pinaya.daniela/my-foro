import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { foroI } from 'src/app/interfaces/usuario.interface';
import { TemasService } from 'src/app/services/temas.service';

@Component({
  selector: 'app-temas',
  templateUrl: './temas.component.html',
  styleUrls: ['./temas.component.css']
})
export class TemasComponent implements OnInit {

  listaTemas:foroI[] = [];
  html:string = '';
  cantidad_mensajes = 0
  foro!: foroI;

  constructor(private _foroService : TemasService,
    private router : Router) {
  
     }

  ngOnInit(): void {
  }

  verForo(){
    this.listaTemas = this._foroService.obtenerTemaLS()
    console.log('Mostro????');
    console.log(this.listaTemas);
    console.log('kjmi');
    
  }
  
  verUnForo(id : number){
    console.log('entra a ver un Foro');
    console.log(id, 'este es el id');
  
    let encontrado = this._foroService.obtenerUnForo(id)
    console.log(encontrado);

    if(encontrado === undefined){
      this.html = ''
    } else{
      console.log("else");
      
      this.html = encontrado.foro;
      this.foro = encontrado
      this.router.navigate(['/dashboard/ver_foro', id])
    }
    console.log(encontrado);
    
    return this.foro
  }
  
  crearForo(number: number){
    this.router.navigate(['/dashboard/crear_foro', number])
  }

}
