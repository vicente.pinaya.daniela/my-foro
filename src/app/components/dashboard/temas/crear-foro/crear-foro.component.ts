import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { foroI } from 'src/app/interfaces/usuario.interface';
import { TemasService } from 'src/app/services/temas.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-foro',
  templateUrl: './crear-foro.component.html',
  styleUrls: ['./crear-foro.component.css']
})
export class CrearForoComponent implements OnInit {

  username!: string;


  seccion: any[] = ['Grupos Masculinos', 'Grupos Femeninos'];

  Foro!: foroI;
  foro = {
    titulo: ''
  }


  html = '<b>Hola</b>';
  editor: Editor; 
  toolbar: Toolbar = [
  ['bold', 'italic'],
  ['underline', 'strike'],
  ['code', 'blockquote'],
  ['ordered_list', 'bullet_list'],
  [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
  ['link', 'image'],
  ['text_color', 'background_color'],
  ['align_left', 'align_center', 'align_right', 'align_justify'],
 ];

  constructor( private _usuariosSvc : UsuarioService,
                private _foroSvc : TemasService,
                private fb: FormBuilder,
                private router : Router) { 
    this.editor = new Editor();


    let foroID = Math.floor(Math.random() * 20)
    console.log(foroID);
    
    let nombre = sessionStorage.getItem('Usuario_nombre');
    console.log(nombre);

    let listaForo : foroI[] = this._usuariosSvc.ObtenerLS()
    console.log(listaForo);
    let coincidencia = listaForo.find(e => e.foroid !== foroID)
    console.log(coincidencia);




  }

  ngOnInit(): void {
  }


  guardar(){
    let fecha = new Date();
    let foroID = Math.floor(Math.random() * 1000)
    console.log(foroID);
    
    let nombre = sessionStorage.getItem('Usuario_nombre');
    console.log(nombre);
    
    let listaForo : foroI[] = this._usuariosSvc.ObtenerLS()
    console.log(listaForo);

    while (listaForo.find(e => e.foroid === foroID)){
      console.log('es igual');
      foroID = Math.floor(Math.random() * 20)
    }
    
    if(nombre === null){
      console.log('no se pudo');
      console.log(nombre);
      
    } else {
      console.log(foroID);
      
      this.Foro = {
        foroid: foroID,
        usuario: nombre,
        seccion: 0,
        titulo: this.foro.titulo,
        fecha: fecha.toLocaleDateString(),
        foro: this.html
      };
      console.log(this.Foro);
      this.subirAlLocal(this.Foro)
    } 
    this.router.navigate(['/dashboard/temas'])
  }

  subirAlLocal(foro :foroI){
    this._foroSvc.agregarTema(foro)
  }

  traerUsuario(){
   let nombre = this._usuariosSvc.mostrarUser()
  return nombre
  }


}
