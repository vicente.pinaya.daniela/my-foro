import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { comentarioI } from 'src/app/interfaces/comentarios.interface';
import { foroI } from 'src/app/interfaces/usuario.interface';
import { ComentariosService } from 'src/app/services/comentarios.service';
import { TemasService } from 'src/app/services/temas.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-foro',
  templateUrl: './ver-foro.component.html',
  styleUrls: ['./ver-foro.component.css']
})
export class VerForoComponent implements OnInit {

comentarios!:comentarioI
foro_id!: number
listcomentarios!: comentarioI[]


  comentario = '<b>Hola</b>';
  editor: Editor; 
  toolbar: Toolbar = [
  ['bold', 'italic'],
  ['underline', 'strike'],
  ['code', 'blockquote'],
  ['ordered_list', 'bullet_list'],
  [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
  ['link', 'image'],
  ['text_color', 'background_color'],
  ['align_left', 'align_center', 'align_right', 'align_justify'],
 ];

  foro!:foroI
  html:string = '';
  constructor(private router:Router,
    private activateRoute: ActivatedRoute,
              private usuarioService: UsuarioService,
              private temasServices: TemasService,
              private comentario_service : ComentariosService
             
    ) { 
      this.editor = new Editor();
    this.activateRoute.params.subscribe(params => {
      const id = params['id'];
      let number: number = Number(id);
      this.foro_id= number
      console.log(this.foro_id);
      
      console.log(number);

      const tema = this.temasServices.obtenerUnForo(number);
      console.log(tema);

      
      if(tema === undefined){
        this.html = 'FORO NO ENCONTRADO'
      } else{
    this.foro = tema;
    this.html = tema.foro;
  } 
    
    });
  }

  ngOnInit(): void {
  }

  guardar(){
    let fecha = new Date();
  
    let nombre = sessionStorage.getItem('Usuario_nombre');
    console.log(nombre);
    
    let listaForo : foroI[] = this.usuarioService.ObtenerLS()
    console.log(listaForo);

    if(nombre === null){
      console.log('no se pudo');
      console.log(nombre);
      
    } else {
  
      
      this.comentarios = {

        usuario: nombre,
        id_foro: this.foro_id,
        comentario:this.comentario,
        fecha: fecha.toLocaleDateString(),
        hora : fecha.toLocaleTimeString(),
      };
      console.log(this.comentarios);
       this.subirAlLocal(this.comentarios)
    } 
  }

  subirAlLocal(comentarios : comentarioI){
    this.comentario_service.agregarcomentario(comentarios)
  }


  Ver(){
    this.listcomentarios = this.comentario_service.obtenerLSdeComentarios()
    console.log(this.listcomentarios);
    console.log('kjmi');
   
  }

  }

  
