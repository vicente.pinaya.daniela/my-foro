import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  forma!: FormGroup;

  listUsuarios: UsuarioDataI[] = [];
 

  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService,
              private _userService:UsuarioService,
              private router : Router) { 
    this.crearFormulario();
    //this.cargarDataAlFormulario();
    // this.cargarDataAlFormulario2();
  }

  ngOnInit(): void {
  }
//GET
  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido() {
    return (this.forma.get('apellido')?.invalid && !this.forma.get('apellido')?.errors?.['noZaralai']) && this.forma.get('apellido')?.touched;
  }
  
  get correoNoValido() {
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }

  get pass1NoValido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2NoValido(){

    //return this.forma.get('pass2')?.hasError('noEsIgual');
    
    const pass1 = this.forma.get('pass1')?.value;
    const pass2 = this.forma.get('pass2')?.value;

    return (pass1 === pass2) ? false : true;

  }

  get usuarioNoValido(){
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioExistente(){
    return this.forma.get('usuario')?.errors?.['existe']
  }

   get emailExistente(){
     return this.forma.get('email')?.errors?.['existe']
   }

  
  crearFormulario(): void {
    this.forma = this.fb.group({
      //Valores del array:
      //1er valor: El valor por defecto que tendra
      //2do valor: Son los validadores sincronos
      //3er valor: Son los validadores asincronos
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario],
      validators: this.validadores.passwordsIguales('pass1', 'pass2')
    } as AbstractControlOptions);
  }

   cargarDataAlFormulario(): void {
  //   this.forma.setValue({
  //     nombre: 'Juan',
  //     apellido: 'Perez',
  //     correo: 'juan@gmail.com',
  //     direccion: {
  //       distrito: 'Central',
  //       ciudad: 'Oruro'
  //     }
  //   });
   }

  // cargarDataAlFormulario2(): void{
  //   this.forma.patchValue({
  //     apellido: 'Perez'
  //   });
  // }

  guardar(): void{
    console.log(this.forma.value);
    console.log('usuario registrado');
    this._userService.agregarUsuario(this.forma.value)
   // this.registrarUsuario();
    this.limpiarFormulario();
    this.router.navigate(['/login'])
    
  }


// AgregarUsuarioList(registro:RegistroI){
//   console.log('empezando local ');
//   if (localStorage.getItem('reunion')===null){
//     this.datos=[]
//     this.datos.push(registro)
//     localStorage.setItem('reunion',JSON.stringify(this.datos))
//   }

//   else { this.datos = JSON.parse(localStorage.getItem('reunion')||'');
//   this.datos.push(registro)
//   localStorage.setItem('reunion',JSON.stringify(this.datos))
// }

// console.log(this.datos);

  
// }

 
  limpiarFormulario(): void {
    //this.forma.reset();
    this.forma.reset({
      // nombre: 'limpieza..pi..pi'
    });
    console.log("limpiado");
    
  }


  mostrar(){
 
    let resultado = JSON.parse(localStorage.getItem('registros')|| '');
    console.log(resultado);
    console.log("entro a mostrar");
    
  
    this.listUsuarios = resultado;
    console.log(this.listUsuarios,"imprimir");

    
}



}


