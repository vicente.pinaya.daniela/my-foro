import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioDataI, UsuarioI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form! : FormGroup;
  loading: boolean = false;
  registro = [];

  constructor(private fb: FormBuilder,
              private router: Router,
              private _usuariosService:UsuarioService) {
       this.formulario();

       this.registro = this._usuariosService.ObtenerLS();
       console.log(this.registro);
        
   }

   ngOnInit(): void {
  }
  
  formulario(): void {
   this.form = this.fb.group({
        usuario: ['', Validators.required],
        password: ['', Validators.required]
      });
  }

  Ingresar(): void {
    console.log(this.form.value);
    const Usuario: UsuarioI = {
      usuario: this.form.value.usuario,
      password: this.form.value.password
    }

    this._usuariosService.logearDatos(Usuario)
    console.log(Usuario);
    const lista:UsuarioDataI[] = JSON.parse(localStorage.getItem("Registros") || '')
    console.log(lista);
    
    
    // if(Usuario.usuario === 'jperez' && Usuario.password === 'admin123'){
    //   //Redireccionamos al dashboard
    //   this.fakeLoading();
    // } else {
    //   //Mostramos un mensaje de error
    //   //this.error();
    //   this.form.reset();
    // }

    if(lista.find(e => e.usuario === Usuario.usuario)){
      //Redireccionamos al dashboard
      console.log('Usuario logeado');
      this.limpiarFormulario();
      this.fakeLoading();
      this._usuariosService.snackbar(2)
      
    
      

  
    }else{
      //Mostramops un mensaje de error
      this._usuariosService.snackbar(1);
      this.form.reset();
    }



  }

  fakeLoading(): void {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
      //Redireccionamos al dashboard
      this.router.navigate(['dashboard']);
    }, 0);
  }

  limpiarFormulario(): void {
    this.form.reset();
  //  this.forma.reset({
  //    nombre: 'Pedro'
  //  });
 }

  

  // error(): void {
  //   this._snackbar.open('usuario o contraseña incorrecto', '', {
  //     duration: 5000,
  //     horizontalPosition: 'center',
  //     verticalPosition: 'bottom'
  //   });
  // }


}
