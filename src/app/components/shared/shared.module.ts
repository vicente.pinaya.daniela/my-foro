import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
//LOGIN
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip'; 
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';

//navbar
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import {MatSortModule} from '@angular/material/sort';
//MENU
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
   MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSliderModule,
    MatSnackBarModule,
    MatCardModule,
    MatTooltipModule,
    MatTableModule,
    MatGridListModule,
    MatSortModule,
    MatFormFieldModule,
    //mwnu
    HttpClientModule
   
   
  ],



  exports:[
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSliderModule,
    MatSnackBarModule,
    MatCardModule,
    MatTooltipModule,
    MatTableModule,
    MatGridListModule,
    MatSortModule,
    MatFormFieldModule,
   
    //menu
    HttpClientModule
    


  ]
})
export class SharedModule { }
