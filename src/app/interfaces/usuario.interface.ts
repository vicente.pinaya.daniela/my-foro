export interface UsuarioI {
  usuario: string;
  password: string;
}

export interface UsuarioDataI {
  usuario: string;
  nombre: string;
  correo:string;
  apellido: string;
  pass1:string;
  pass2:string;

  // sexo: string;
}

export interface foroI {
  foroid: number,
  usuario: String,
  seccion: number,
  titulo: string,
  fecha: string,
  foro: string,
}

export interface CommentI {
  foroid: number,
  usuario:string,
  
}